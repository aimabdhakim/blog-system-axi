package controllers

import (
	"blog-system-axi/models"
	"blog-system-axi/utils"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

var NewBlog models.Blog

func CreateBlog(w http.ResponseWriter, r *http.Request) {
	b := &models.Blog{}
	utils.ParseBody(r, b)
	b.CreateBlog()
	respondWithJSON(w, http.StatusOK, b)
}

func GetAllBlog(w http.ResponseWriter, r *http.Request) {
	tag := r.URL.Query().Get("tag")
	var b []models.Blog
	if tag == "" {
		b, _ = models.GetAllBlog()
	} else {
		b, _ = models.GetBlogByTag(tag)
	}

	respondWithJSON(w, http.StatusOK, b)
}

func GetBlog(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	blogID := vars["blogId"]
	b := models.Blog{BlogID: blogID}
	err := b.GetBlog()
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid Blog ID")
		return
	}

	respondWithJSON(w, http.StatusOK, b)
}

func UpdateBlog(w http.ResponseWriter, r *http.Request) {
	var updateBlog = &models.Blog{}
	utils.ParseBody(r, updateBlog)

	vars := mux.Vars(r)
	blogID := vars["blogId"]

	b := models.Blog{BlogID: blogID}
	err := b.GetBlog()
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid Blog ID")
		return
	}

	if updateBlog.Title != "" {
		b.Title = updateBlog.Title
	}
	if updateBlog.Content != "" {
		b.Content = updateBlog.Content
	}
	if len(updateBlog.Tags) > 0 {
		b.Tags = updateBlog.Tags
	}
	if updateBlog.Status != 0 {
		b.Status = updateBlog.Status
	}
	b.PublishDate = time.Now()

	err = b.UpdateBlog()
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Failed to Update Blog")
		return
	}
	respondWithJSON(w, http.StatusOK, b)
}

func DeleteBlog(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	blogID := vars["blogId"]

	b := models.Blog{BlogID: blogID}

	err := b.GetBlog()
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid Blog ID")
		return
	}

	err = b.DeleteBlog()
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Failed to Delete Blog")
		return
	}

	respondWithJSON(w, http.StatusOK, b)
}

func GetBlogByTag(w http.ResponseWriter, r *http.Request) {
	tag := r.URL.Query().Get("tag")
	fmt.Println(tag)
	b, _ := models.GetBlogByTag(tag)
	respondWithJSON(w, http.StatusOK, b)
}

func respondWithError(w http.ResponseWriter, code int, message string) {
	respondWithJSON(w, code, map[string]string{"error": message})
}

func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}
