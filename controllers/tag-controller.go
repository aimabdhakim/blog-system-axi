package controllers

import (
	"blog-system-axi/models"
	"blog-system-axi/utils"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

var NewTag models.Tag

func CreateTag(w http.ResponseWriter, r *http.Request) {
	t := &models.Tag{}
	utils.ParseBody(r, t)
	err := t.CreateTag()
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Tag already exists")
		return
	}
	respondWithJSON(w, http.StatusOK, t)
}

func GetALlTag(w http.ResponseWriter, r *http.Request) {
	t, _ := models.GetAllTag()
	respondWithJSON(w, http.StatusOK, t)
}

func GetTag(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	tagID := vars["tagId"]
	varTagID, err := strconv.Atoi(tagID)
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid Tag ID")
		return
	}
	t := models.Tag{TagID: varTagID}
	err = t.GetTag()
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid Tag ID")
		return
	}

	respondWithJSON(w, http.StatusOK, t)
}

func UpdateTag(w http.ResponseWriter, r *http.Request) {
	var updateTag = &models.Tag{}
	utils.ParseBody(r, updateTag)

	vars := mux.Vars(r)
	tagID := vars["tagId"]
	varTagID, err := strconv.Atoi(tagID)
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Tag ID type is invalid")
		return
	}
	t := models.Tag{TagID: varTagID}
	fmt.Println(t.TagID)
	err = t.GetTag()
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid Tag ID")
		return
	}

	if updateTag.Label != "" {
		t.Label = updateTag.Label
	}

	err = t.UpdateTag()
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Failed to Update Blog")
		return
	}
	respondWithJSON(w, http.StatusOK, t)
}

func DeleteTag(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	tagID := vars["tagId"]
	varTagID, err := strconv.Atoi(tagID)
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid Tag ID")
		return
	}
	t := models.Tag{TagID: varTagID}

	err = t.GetTag()
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid Blog ID")
		return
	}

	err = t.DeleteTag()
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Failed to Delete Blog")
		return
	}

	respondWithJSON(w, http.StatusOK, t)
}
