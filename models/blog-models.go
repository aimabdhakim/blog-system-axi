package models

import (
	"blog-system-axi/config"
	"database/sql"
	"time"

	"github.com/lib/pq"
)

var db *sql.DB

type Blog struct {
	BlogID      string         `json:"blogid"`
	Title       string         `json:"title"`
	Content     string         `json:"content"`
	Tags        pq.StringArray `json:"tags"`
	Status      int            `json:"status"`
	PublishDate time.Time      `json:"publishdate"`
}

func init() {
	config.Connect()
	db = config.GetDB()
}

func (b *Blog) GetBlog() error {
	sqlQuery := "SELECT title, content, tags, status, publishdate FROM blog WHERE blogid=$1"
	err := db.QueryRow(sqlQuery, b.BlogID).Scan(&b.Title,
		&b.Content, &b.Tags, &b.Status, &b.PublishDate)
	return err
}

func (b *Blog) CreateBlog() error {
	b.PublishDate = time.Now()
	sqlQuery := "INSERT INTO blog(title, content, tags, status, publishdate) VALUES ($1,$2,$3,$4,$5) RETURNING blogid"
	err := db.QueryRow(sqlQuery, b.Title, b.Content, pq.Array(b.Tags), b.Status, b.PublishDate).Scan(&b.BlogID)
	if err != nil {
		return err
	}

	return nil
}

func GetAllBlog() ([]Blog, error) {
	sqlQuery := "SELECT blogid, title, content, tags, status, publishdate FROM blog"
	rows, err := db.Query(sqlQuery)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	blogs := []Blog{}
	for rows.Next() {
		var b Blog
		if err := rows.Scan(&b.BlogID, &b.Title, &b.Content, &b.Tags, &b.Status, &b.PublishDate); err != nil {
			return nil, err
		}
		blogs = append(blogs, b)
	}

	return blogs, nil
}

func (b *Blog) UpdateBlog() error {
	sqlQuery := "UPDATE blog SET title=$2, content=$3, tags=$4, status=$5, publishdate=$6 WHERE blogid=$1"
	_, err := db.Exec(sqlQuery, b.BlogID, b.Title, b.Content, b.Tags, b.Status, b.PublishDate)
	return err
}

func (b *Blog) DeleteBlog() error {
	sqlQuery := "DELETE FROM blog WHERE blogid=$1"
	_, err := db.Exec(sqlQuery, b.BlogID)
	return err
}

func GetBlogByTag(tag string) ([]Blog, error) {
	arrTag := []string{tag}
	sqlQuery := "SELECT blogid, title, content, tags, status, publishdate FROM blog WHERE $1 <@ tags"
	rows, err := db.Query(sqlQuery, pq.Array(arrTag))
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	blogs := []Blog{}
	for rows.Next() {
		var b Blog
		if err := rows.Scan(&b.BlogID, &b.Title, &b.Content, &b.Tags, &b.Status, &b.PublishDate); err != nil {
			return nil, err
		}
		blogs = append(blogs, b)
	}

	return blogs, nil
}
