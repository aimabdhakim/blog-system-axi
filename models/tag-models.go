package models

import (
	"blog-system-axi/config"
	"fmt"
)

type Tag struct {
	TagID int    `json:"tagid"`
	Label string `json:"label"`
}

func init() {
	config.Connect()
	db = config.GetDB()
}

func (t *Tag) GetTag() error {
	sqlQuery := "SELECT label FROM tag WHERE tagid=$1"
	err := db.QueryRow(sqlQuery, t.TagID).Scan(&t.Label)
	return err
}

func (t *Tag) CreateTag() error {
	err := t.GetTag()
	if t.TagID != 0 {
		return err
	}

	sqlQuery := "INSERT INTO tag(label) VALUES ($1) RETURNING tagid"
	err = db.QueryRow(sqlQuery, t.Label).Scan(&t.TagID)
	fmt.Println(err)
	if err != nil {
		return err
	}

	return nil
}

func GetAllTag() ([]Tag, error) {
	sqlQuery := "SELECT tagid, label FROM tag"
	rows, err := db.Query(sqlQuery)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	tags := []Tag{}

	for rows.Next() {
		var t Tag
		if err := rows.Scan(&t.TagID, &t.Label); err != nil {
			return nil, err
		}
		tags = append(tags, t)
	}

	return tags, nil
}

func (t *Tag) UpdateTag() error {
	sqlQuery := "UPDATE tag SET label=$2 WHERE tagid=$1"
	_, err := db.Exec(sqlQuery, t.TagID, t.Label)
	return err
}

func (t *Tag) DeleteTag() error {
	sqlQuery := "DELETE FROM tag WHERE tagid=$1"
	_, err := db.Exec(sqlQuery, t.TagID)
	return err
}

func getTagByLabel(label string) error {
	return nil
}
