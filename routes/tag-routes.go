package routes

import (
	"github.com/gorilla/mux"

	"blog-system-axi/controllers"
)

var TagRoutes = func(router *mux.Router) {
	router.HandleFunc("/api/tags", controllers.GetALlTag).Methods("GET")
	router.HandleFunc("/api/tag", controllers.CreateTag).Methods("POST")
	router.HandleFunc("/api/tag/{tagId}", controllers.UpdateTag).Methods("PUT")
	router.HandleFunc("/api/tag/{tagId}", controllers.GetTag).Methods("GET")
	router.HandleFunc("/api/tag/{tagId}", controllers.DeleteTag).Methods("DELETE")
}
