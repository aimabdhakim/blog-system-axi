package routes

import (
	"github.com/gorilla/mux"

	"blog-system-axi/controllers"
)

var BlogRoutes = func(router *mux.Router) {
	router.HandleFunc("/api/post", controllers.CreateBlog).Methods("POST")
	router.HandleFunc("/api/posts", controllers.GetAllBlog).Methods("GET")
	router.HandleFunc("/api/post/{blogId}", controllers.UpdateBlog).Methods("PUT")
	router.HandleFunc("/api/post/{blogId}", controllers.GetBlog).Methods("GET")
	router.HandleFunc("/api/post/{blogId}", controllers.DeleteBlog).Methods("DELETE")
}
