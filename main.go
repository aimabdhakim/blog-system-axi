package main

import (
	"fmt"
	"log"
	"net/http"

	"blog-system-axi/routes"

	"github.com/gorilla/mux"
)

func main() {
	r := mux.NewRouter()
	routes.BlogRoutes(r)
	routes.TagRoutes(r)

	http.Handle("/", r)
	fmt.Println("listening on port 8080....")
	log.Fatal(http.ListenAndServe(":8080", r))
}
